﻿using System;
using System.Runtime.InteropServices;

// nm libassimp-fat.a | grep " T _ai" | sed -r 's/^[^_]*_(.*)$/\t[DllImport("__Internal")]\n\tinternal static extern IntPtr \1();\n/'

namespace AssimpNet.iOS
{
    internal class ai
    {
	[DllImport("__Internal")]
	internal static extern IntPtr aiApplyPostProcessing();

	[DllImport("__Internal")]
	internal static extern IntPtr aiAttachLogStream();

	[DllImport("__Internal")]
	internal static extern IntPtr aiCreatePropertyStore();

	[DllImport("__Internal")]
	internal static extern IntPtr aiCreateQuaternionFromMatrix();

	[DllImport("__Internal")]
	internal static extern IntPtr aiDecomposeMatrix();

	[DllImport("__Internal")]
	internal static extern IntPtr aiDetachAllLogStreams();

	[DllImport("__Internal")]
	internal static extern IntPtr aiDetachLogStream();

	[DllImport("__Internal")]
	internal static extern IntPtr aiEnableVerboseLogging();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetErrorString();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetExtensionList();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetImportFormatCount();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetImportFormatDescription();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetImporterDesc();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetMemoryRequirements();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetPredefinedLogStream();

	[DllImport("__Internal")]
	internal static extern IntPtr aiIdentityMatrix3();

	[DllImport("__Internal")]
	internal static extern IntPtr aiIdentityMatrix4();

	[DllImport("__Internal")]
	internal static extern IntPtr aiImportFile();

	[DllImport("__Internal")]
	internal static extern IntPtr aiImportFileEx();

	[DllImport("__Internal")]
	internal static extern IntPtr aiImportFileExWithProperties();

	[DllImport("__Internal")]
	internal static extern IntPtr aiImportFileFromMemory();

	[DllImport("__Internal")]
	internal static extern IntPtr aiImportFileFromMemoryWithProperties();

	[DllImport("__Internal")]
	internal static extern IntPtr aiIsExtensionSupported();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix3AreEqual();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix3AreEqualEpsilon();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix3Determinant();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix3FromMatrix4();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix3FromQuaternion();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix3FromRotationAroundAxis();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix3FromTo();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix3Inverse();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix3RotationZ();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix3Translation();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4Add();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4AreEqual();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4AreEqualEpsilon();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4DecomposeIntoScalingAxisAnglePosition();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4DecomposeIntoScalingEulerAnglesPosition();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4DecomposeNoScaling();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4Determinant();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4FromEulerAngles();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4FromMatrix3();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4FromRotationAroundAxis();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4FromScalingQuaternionPosition();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4FromTo();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4Inverse();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4IsIdentity();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4RotationX();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4RotationY();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4RotationZ();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4Scaling();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMatrix4Translation();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMultiplyMatrix3();

	[DllImport("__Internal")]
	internal static extern IntPtr aiMultiplyMatrix4();

	[DllImport("__Internal")]
	internal static extern IntPtr aiQuaternionAreEqual();

	[DllImport("__Internal")]
	internal static extern IntPtr aiQuaternionAreEqualEpsilon();

	[DllImport("__Internal")]
	internal static extern IntPtr aiQuaternionConjugate();

	[DllImport("__Internal")]
	internal static extern IntPtr aiQuaternionFromAxisAngle();

	[DllImport("__Internal")]
	internal static extern IntPtr aiQuaternionFromEulerAngles();

	[DllImport("__Internal")]
	internal static extern IntPtr aiQuaternionFromNormalizedQuaternion();

	[DllImport("__Internal")]
	internal static extern IntPtr aiQuaternionInterpolate();

	[DllImport("__Internal")]
	internal static extern IntPtr aiQuaternionMultiply();

	[DllImport("__Internal")]
	internal static extern IntPtr aiQuaternionNormalize();

	[DllImport("__Internal")]
	internal static extern IntPtr aiReleaseImport();

	[DllImport("__Internal")]
	internal static extern IntPtr aiReleasePropertyStore();

	[DllImport("__Internal")]
	internal static extern IntPtr aiSetImportPropertyFloat();

	[DllImport("__Internal")]
	internal static extern IntPtr aiSetImportPropertyInteger();

	[DllImport("__Internal")]
	internal static extern IntPtr aiSetImportPropertyMatrix();

	[DllImport("__Internal")]
	internal static extern IntPtr aiSetImportPropertyString();

	[DllImport("__Internal")]
	internal static extern IntPtr aiTransformVecByMatrix3();

	[DllImport("__Internal")]
	internal static extern IntPtr aiTransformVecByMatrix4();

	[DllImport("__Internal")]
	internal static extern IntPtr aiTransposeMatrix3();

	[DllImport("__Internal")]
	internal static extern IntPtr aiTransposeMatrix4();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2Add();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2AreEqual();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2AreEqualEpsilon();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2DivideByScalar();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2DivideByVector();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2DotProduct();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2Length();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2Negate();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2Normalize();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2Scale();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2SquareLength();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2Subtract();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector2SymMul();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3Add();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3AreEqual();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3AreEqualEpsilon();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3CrossProduct();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3DivideByScalar();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3DivideByVector();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3DotProduct();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3Length();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3LessThan();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3Negate();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3Normalize();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3NormalizeSafe();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3RotateByQuaternion();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3Scale();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3SquareLength();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3Subtract();

	[DllImport("__Internal")]
	internal static extern IntPtr aiVector3SymMul();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetBranchName();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetCompileFlags();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetLegalString();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetVersionMajor();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetVersionMinor();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetVersionPatch();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetVersionRevision();

	[DllImport("__Internal")]
	internal static extern IntPtr aiTextureTypeToString();

	[DllImport("__Internal")]
	internal static extern IntPtr aiCopyScene();

	[DllImport("__Internal")]
	internal static extern IntPtr aiExportScene();

	[DllImport("__Internal")]
	internal static extern IntPtr aiExportSceneEx();

	[DllImport("__Internal")]
	internal static extern IntPtr aiExportSceneToBlob();

	[DllImport("__Internal")]
	internal static extern IntPtr aiFreeScene();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetExportFormatCount();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetExportFormatDescription();

	[DllImport("__Internal")]
	internal static extern IntPtr aiReleaseExportBlob();

	[DllImport("__Internal")]
	internal static extern IntPtr aiReleaseExportFormatDescription();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetMaterialColor();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetMaterialFloatArray();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetMaterialIntegerArray();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetMaterialProperty();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetMaterialString();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetMaterialTexture();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetMaterialTextureCount();

	[DllImport("__Internal")]
	internal static extern IntPtr aiGetMaterialUVTransform();
    }
}

