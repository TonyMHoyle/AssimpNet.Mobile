﻿/*
* Copyright (c) 2012-2020 AssimpNet - Nicholas Woodfield
* Copyright (c) 2022 Tony Hoyle
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/


using System;
using System.IO;
using Assimp;
using Assimp.Unmanaged;
using UnityEngine;

using RuntimePlatform = Assimp.Unmanaged.RuntimePlatform;
using UnityPlatform = UnityEngine.RuntimePlatform;

namespace AssimpNet.Unity
{
    internal class UnityData
    {
        public UnityPlatform platform { get; set; }
        public string pluginsFolder { get; set; }
        public string editorPluginNativeFolder { get; set; }
        public UnityData()
        {
            pluginsFolder = Path.Combine(Application.dataPath, "Plugins");
            string? assemblyFolder = Path.GetDirectoryName(GetCallerFilePath());

            if(!Directory.Exists(pluginsFolder) && !string.IsNullOrEmpty(assemblyFolder))
                pluginsFolder = Directory.GetParent(assemblyFolder).FullName;

            editorPluginNativeFolder = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "../Native");

            if (!Directory.Exists(editorPluginNativeFolder) && assemblyFolder != null)
                editorPluginNativeFolder = Path.Combine(assemblyFolder, "Native");
        }
        private static string GetCallerFilePath([System.Runtime.CompilerServices.CallerFilePath] string filepath = "")
        {
            return filepath;
        }
    }
    internal class UnityRuntimePlatform : RuntimePlatform
    {
        public override UnmanagedLibraryImplementation CreateRuntimeLibraryImplementation(UnmanagedLibraryResolver resolver, string defaultLibName, Type[] unmanagedFunctionDelegateTypes)
        {
            if(Application.platform == UnityPlatform.WebGLPlayer)
                return new UnmanagedUnityWebGLLibraryImplementation(defaultLibName, unmanagedFunctionDelegateTypes);
            if(Application.platform == UnityPlatform.IPhonePlayer)
                return new UnmanagedUnityiOSLibraryImplementation(defaultLibName, unmanagedFunctionDelegateTypes);
            if (Application.platform == UnityPlatform.Android)
                return new UnmanagedUnityAndroidLibraryImplementation(defaultLibName, unmanagedFunctionDelegateTypes);

            var folders = new UnityData();

            if(Application.platform == UnityPlatform.WindowsEditor || Application.platform == UnityPlatform.WindowsPlayer)
                return new UnmanagedUnityWin32LibraryImplementation(resolver,defaultLibName, unmanagedFunctionDelegateTypes, folders);
            if (Application.platform == UnityPlatform.LinuxEditor || Application.platform == UnityPlatform.LinuxPlayer)
                return new UnmanagedUnityLinuxLibraryImplementation(resolver, defaultLibName, unmanagedFunctionDelegateTypes, folders);
            if (Application.platform == UnityPlatform.OSXEditor || Application.platform == UnityPlatform.OSXPlayer)
                return new UnmanagedUnityMacLibraryImplementation(resolver, defaultLibName, unmanagedFunctionDelegateTypes, folders);
            throw new AssimpException("Unsupported unity platform");
        }

        public override Assimp.Unmanaged.Platform GetPlatform()
        {
            return Assimp.Unmanaged.Platform.Other;
        }
    }
}
